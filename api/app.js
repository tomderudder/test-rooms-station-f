'use strict';

const express = require('express')
    // , mongodb = require('mongodb').MongoClient
    , mongodb = require('mongodb')
    , config = require('config')
    , bodyParser = require('body-parser')
    , helmet = require('helmet')
    , rootFilePath = __dirname + '/roots.js'
    , app = express();

let database = {};

dbConnection()
    .then(() => {
        loadMiddlewares();
        loadRoots();
        listen();
    })
    .catch(err => console.error(err))

function dbConnection() {
    const url = 'mongodb://' + config.db.host + ':' + config.db.port + '/' + config.db.name;

    return new Promise((next, reject) =>
        mongodb.connect(url, { useNewUrlParser: true }, (err, db) => {
            if (err)
                reject('The connection to the database ' + config.db.name + ' failed', err);

            database = db.db(config.db.name);
            next();
        }));
}

function loadRoots() {
    require(rootFilePath)(mongodb, app, database);
}

function loadMiddlewares() {

    // body-parser
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    app.use(bodyParser.raw())
    app.use(bodyParser.text())

    // cros origin
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "http://" + config.site.host + ":" + config.site.port);
        res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        res.header("Access-Control-Allow-Credentials", "true");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, mode");
        'OPTIONS' === req.method ? res.sendStatus(200) : next();
    })

    // helmet
    app.use(helmet());
}

function listen() {
    app.listen(config.api.port, config.api.host, () =>
        console.log('Api listening on port ' + config.api.port + ' at ' + config.api.host));
}