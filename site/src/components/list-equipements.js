import React, { Component } from 'react';

class ListEquipements extends Component {
    constructor(props) {
        super(props);

        this.state = {
            err: null,
            isLoaded: false,
            equipements: []
        };
    }

    componentDidMount() {
        fetch("http://localhost:3000/equipements")
            .then(res => res.json())
            .then(equipements => {
                this.setState({
                    isLoaded: true,
                    equipements: equipements
                })
            },
                err =>
                    this.setState({
                        isLoaded: true,
                        err
                    }));
    }

    render() {
        const { error, isLoaded, equipements } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <label>
                    <span className="up">Équipements souhaités</span>
                    <div className="list" >
                        <div className={"center btn btn-stroke" + (this.props.selection.length === 0 ? " btn-ligth" : "")} onClick={this.props.parentMethod.bind(this, -1)}>Aucun | Tous</div>
                        {equipements.map((equipement, key) => (
                            <div className={"center btn btn-stroke" + (this.props.selection.indexOf(equipement) !== -1 ? " btn-ligth" : "")} key={key} onClick={this.props.parentMethod.bind(this, key)}>
                                {equipement}
                            </div>
                        ))}
                    </div>
                </label>
            );
        }
    }
}

export default ListEquipements;