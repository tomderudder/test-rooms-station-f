import React, { Component } from 'react';
import './range.css';

class Range extends Component {

    render() {
        return (
            <label>
                <span className="up">{this.props.children}</span>
                <range type="basic">
                    <input className="square" type="range" value={this.props.value} min="1" max={this.props.max} onChange={this.props.parentMethod} />
                </range>
            </label>
        );
    }
}

export default Range;